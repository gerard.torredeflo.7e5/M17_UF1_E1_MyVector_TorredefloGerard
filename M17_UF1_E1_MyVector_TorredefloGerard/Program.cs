﻿using System;

namespace M17_UF1_E1_MyVector_TorredefloGerard
{
    class Program
    {
        static void Main(string[] args)
        {
            bool run = true;
            while (run)
            {
                Console.WriteLine("------------------------------------------\nEscull que vols fer:\n1.Dir Hola\n2.Escriure Vector\n3.Generar una array de vectors aleatoris\n4.Sortir");
                int opc = Convert.ToInt32(Console.ReadLine());
                switch (opc)
                {
                    case 1:
                        Hello();
                        break;
                    case 2:
                        Vector();
                        break;
                    case 3:
                        Console.WriteLine("Escriu el tamany de l'array");
                        int lenght = Convert.ToInt32(Console.ReadLine());
                        MyVector[] array = VectorGame.randomVectors(lenght);
                        bool ordre = true;
                        while (ordre)
                        {
                            Console.WriteLine("Vols: \n1.Ordenar el vector per distancia \no\n2.per distància a l'origen\n 3.Sortir.");
                            int eleccio = Convert.ToInt32(Console.ReadLine());
                            switch (eleccio)
                            {
                                case 1:
                                    VectorGame.SortVectors(array, true);
                                    break;
                                case 2:
                                    VectorGame.SortVectors(array, false);
                                    break;
                                case 3:
                                    ordre = false;
                                    break;
                                default:
                                    Console.WriteLine("No és una de les opcions");
                                    break;
                            }
                        }
                        break;
                    case 4:
                        run = false;
                        break;
                    default:
                        Console.WriteLine("No és una de les opcions");
                        break;
                }
            }
        }

        static void Hello()
        {
            String name;
            Console.WriteLine("Escriu el teu nom: ");
            name = Console.ReadLine();
            Console.WriteLine("Hello {0}", name);
        }

        static void Vector()
        {
            Console.WriteLine("Escriu coordenades 2D (x,y): ");
            int x1 = Convert.ToInt32(Console.ReadLine());
            int y1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Escriu unes altres coordenades 2D (x,y): ");
            int x2 = Convert.ToInt32(Console.ReadLine());
            int y2 = Convert.ToInt32(Console.ReadLine());

            MyVector myVector = new MyVector(x1, x2, y1, y2);

            myVector.CalculateVector();
            myVector.ChangeDirection();

            Console.WriteLine("\nMyVector:\n" + myVector);
        }


    }

    class MyVector
    {
        public double x1 { get; set; }
        public double x2 { get; set; }
        public double y1 { get; set; }
        public double y2 { get; set; }
        public double xy { get; set; }
        public double xyneg;

        public MyVector() { }

        public MyVector(double x1, double x2, double y1, double y2)
        {
            this.x1 = x1;
            this.x2 = x2;
            this.y1 = y1;
            this.y2 = y2;
        }

        public void CalculateVector()
        {
            xy = Math.Sqrt(Math.Pow((x2 - x1), 2) + Math.Pow((y2 - y1), 2));
        }

        public void ChangeDirection()
        {
            xyneg = -xy;
        }

        public override string ToString()
        {
            return "Coordenada A: (" + x1 + ", " + y1 + ")\n"
                + "Coordenada B: (" + x2 + ", " + y2 + ")\n"
                + "Distància vector: " + xy + "\n"
                + "Distància vector, canviar direcció: " + xyneg + "\n";
        }
    }
    class VectorGame
    {
        public static MyVector[] randomVectors(int lenght)
        {
            Console.WriteLine("Random vector array: ");
            Random random = new Random();
            MyVector[] arrayVector = new MyVector[lenght];
            MyVector vector;

            for (int i = 0; i < lenght; i++)
            {
                vector = new MyVector(random.Next(-99, 99), random.Next(-99, 99), random.Next(-99, 99), random.Next(-99, 99));
                arrayVector[i] = vector;
                vector.CalculateVector();
                vector.ChangeDirection();
            }
            foreach (MyVector i in arrayVector)
            {
                Console.WriteLine(i);
            }

            return arrayVector;
        }

        public static MyVector[] SortVectors(MyVector[] x, bool opt)
        {
            MyVector[] arrayOrdenada = x;
            if (opt == true)
            {
                MyVector temp;
                for (int i = 0; i < arrayOrdenada.Length - 1; i++)
                {
                    for (int j = 0; j < arrayOrdenada.Length - 1; j++)
                    {
                        if (x[j].xy < x[j + 1].xy)
                        {
                            temp = arrayOrdenada[j + 1];
                            arrayOrdenada[j + 1] = arrayOrdenada[j];
                            arrayOrdenada[j] = temp;
                        }
                    }
                }
                Console.WriteLine("\nOrdenar per distància de més gran a més petit: ");
                foreach (MyVector i in arrayOrdenada)
                {
                    Console.WriteLine(i);
                }
            }
            else
            {
                double[] origin = new double[x.Length];
                MyVector temp;
                double dista;
                double distb;
                for (int i = 0; i < origin.Length; i++)
                {
                    dista = Math.Sqrt(Math.Pow((x[i].x1), 2) + Math.Pow((x[i].y1), 2));
                    distb = Math.Sqrt(Math.Pow((x[i].x2), 2) + Math.Pow((x[i].y2), 2));
                    if (dista < distb)
                    {
                        origin[i] = dista;
                    }
                    else
                    {
                        origin[i] = distb;
                    }
                }
                for (int i = 0; i < arrayOrdenada.Length + 1; i++)
                {
                    for (int j = i + 1; j < arrayOrdenada.Length; j++)
                    {
                        if (origin[i] > origin[j])
                        {
                            temp = arrayOrdenada[i];
                            arrayOrdenada[i] = arrayOrdenada[j];
                            arrayOrdenada[j] = temp;
                        }
                    }
                }
                // Mostra per pantalla
                Console.WriteLine("\nOrdenar per distància des d'origen de més petit a més gran: ");
                foreach (MyVector i in arrayOrdenada)
                {
                    Console.WriteLine(i);
                }
            }
            return arrayOrdenada;
        }
    }
}

